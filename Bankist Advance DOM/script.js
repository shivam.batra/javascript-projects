"use strict";

// all DOM Variable
const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".btn--close-modal");
const btnsOpenModal = document.querySelectorAll(".btn--show-modal");

const btnScrollTo = document.querySelector(".btn--scroll-to");
const section1 = document.querySelector("#section--1");
const nav = document.querySelector(".nav");

const tabs = document.querySelectorAll(".operations__tab");
const tabsContainer = document.querySelector(".operations__tab-container");
const tabsContent = document.querySelectorAll(".operations__content");

const openModal = function () {
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
};

const closeModal = function () {
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
};

for (let i = 0; i < btnsOpenModal.length; i++)
  btnsOpenModal[i].addEventListener("click", openModal);

btnCloseModal.addEventListener("click", closeModal);
overlay.addEventListener("click", closeModal);

document.addEventListener("keydown", function (e) {
  if (e.key === "Escape" && !modal.classList.contains("hidden")) {
    closeModal();
  }
});
/////////////////////////////////////////////////////////
///// SCROLLING EFFECT //////

// Button Scrolling
btnScrollTo.addEventListener("click", function (event) {
  const s1coords = section1.getBoundingClientRect();

  // for scrolling
  // window.scrollTo(s1coords.left, s1coords.top + window.pageYOffset);

  // for smooth scrolling
  // window.scrollTo({
  //   left: s1coords.left + window.pageXOffset,
  //   top: s1coords.top + window.pageYOffset,
  //   behavior: "smooth",
  // });

  // easy method
  section1.scrollIntoView({ behavior: "smooth" });
});

// adding event listener in each nav-link
// document.querySelectorAll(".nav__link").forEach(function (eventListner) {
//   eventListner.addEventListener("click", function (event) {
//     event.preventDefault();
//     const id = this.getAttribute("href");
//     console.log(id);
//     document.querySelector(id).scrollIntoView({ behavior: "smooth" });
//   });
// });

// scrolling using event delegation
// 1. Add event listener to common parent
// 2. Determine what element originated the event
/////////////////////////////////////////////////////
document
  .querySelector(".nav__links")
  .addEventListener("click", function (event) {
    event.preventDefault();
    // matching strategy
    if (event.target.classList.contains("nav__link")) {
      const id = event.target.getAttribute("href");
      document.querySelector(id).scrollIntoView({ behavior: "smooth" });
    }
  });
////////////////////////////////////////////////////
// tabbed component
tabsContainer.addEventListener("click", function (event) {
  const clicked = event.target.closest(".operations__tab");

  // Gaurd Clause
  if (!clicked) return;

  // remove active class
  tabs.forEach(function (tab) {
    tab.classList.remove("operations__tab--active");
  });
  // active tab
  clicked.classList.add("operations__tab--active");

  //
  tabsContent.forEach(function (tab) {
    tab.classList.remove("operations__content--active");
  });
  // activate content area
  document
    .querySelector(`.operations__content--${clicked.dataset.tab}`)
    .classList.add("operations__content--active");
});
////////////////////////////////////////////////////
// Menu Fade Animation
const handleHover = function (event) {
  if (event.target.classList.contains("nav__link")) {
    const link = event.target;
    const siblings = link.closest(".nav").querySelectorAll(".nav__link");
    const logo = link.closest(".nav").querySelector("img");

    const opacity = this;
    siblings.forEach(function (element) {
      if (element !== link) {
        element.style.opacity = opacity;
      }
    });
    logo.style.opacity = opacity;
  }
};

nav.addEventListener("mouseover", handleHover.bind(0.5));
nav.addEventListener("mouseout", handleHover.bind(1));

// Sticky navigation : old method

// const initialCoords = section1.getBoundingClientRect();
// console.log(initialCoords.top);
// window.addEventListener("scroll", function () {
//   if (window.scrollY > initialCoords.top) nav.classList.add("sticky");
//   else nav.classList.remove("sticky");
// });
//////////////////////////////////////////////////////
// Sticky navigation : using intersectionObserver API
// const obsCallBack = function (entries, observer) {
//   entries.forEach((entry) => {
//     console.log(entry);
//   });
// };

// const obsOptions = {
//   root: null,
//   threshold: [0, 1, 0.2],
// };

// const observer = new IntersectionObserver(obsCallBack, obsOptions);
// observer.observe(section1);

const header = document.querySelector(".header");
const navHeight = nav.getBoundingClientRect().height;

const stickyNav = function (entries) {
  const [entry] = entries;
  if (!entry.isIntersecting) nav.classList.add("sticky");
  else nav.classList.remove("sticky");
};
const headerObserver = new IntersectionObserver(stickyNav, {
  root: null,
  threshold: 0,
  rootMargin: `-${navHeight}px`,
});
headerObserver.observe(header);
//////////////////////////////////////////////////////
// section visible effect on scrolling
const allSections = document.querySelectorAll(".section");

const revealSections = function (entries, observer) {
  const [entry] = entries;

  if (!entry.isIntersecting) return;
  entry.target.classList.remove("section--hidden");
  observer.unobserve(entry.target);
};
const sectionObserver = new IntersectionObserver(revealSections, {
  root: null,
  threshold: 0.15,
});

allSections.forEach(function (section) {
  sectionObserver.observe(section);
  section.classList.add("section--hidden");
});

///////////////////////////////////////
// Lazy loading images
const imgTargets = document.querySelectorAll("img[data-src]");

const loadImg = function (entries, observer) {
  const [entry] = entries;

  // gaurd clause
  if (!entry.isIntersecting) return;

  entry.target.src = entry.target.dataset.src;
  entry.target.addEventListener("load", function () {
    entry.target.classList.remove("lazy-img");
  });

  observer.unobserve(entry.target);
};
const imgObserver = new IntersectionObserver(loadImg, {
  root: null,
  threshold: 0,
  rootMargin: "-200px",
});

imgTargets.forEach((img) => imgObserver.observe(img));
///////////////////////////////////////////////////////

// slider component
const sliders = function () {
  const slides = document.querySelectorAll(".slide");
  const slider = document.querySelector(".slider");
  const btnLeft = document.querySelector(".slider__btn--left");
  const btnRight = document.querySelector(".slider__btn--right");
  const dotContainer = document.querySelector(".dots");
  let currSlide = 0;
  const maxSlide = slides.length;

  // required functions
  const createDots = function () {
    slides.forEach(function (_, i) {
      dotContainer.insertAdjacentHTML(
        "beforeend",
        `<button class="dots__dot" data-slide="${i}"></button>`
      );
    });
  };
  const goToSlide = function (slide) {
    slides.forEach(function (s, i) {
      s.style.transform = `translateX(${100 * (i - slide)}%)`;
    });
  };
  const prevSlide = function () {
    if (currSlide === 0) currSlide = maxSlide - 1;
    else currSlide--;
    goToSlide(currSlide);
    activateDots(currSlide);
  };
  const nextSlide = function () {
    if (currSlide === maxSlide - 1) {
      currSlide = 0;
    } else currSlide++;
    goToSlide(currSlide);
    activateDots(currSlide);
  };
  const activateDots = function (slide) {
    document.querySelectorAll(".dots__dot").forEach(function (dot) {
      dot.classList.remove("dots__dot--active");
    });

    document
      .querySelector(`.dots__dot[data-slide="${slide}"]`)
      .classList.add("dots__dot--active");
  };

  const init = function () {
    createDots();
    goToSlide(0);
    activateDots(0);
  };

  // calling functions initially
  init();

  // event listener
  btnRight.addEventListener("click", nextSlide);
  btnLeft.addEventListener("click", prevSlide);

  document.addEventListener("keydown", function (event) {
    if (event.key === "ArrowLeft") prevSlide();
    event.key === "ArrowRight" && nextSlide();
  });

  dotContainer.addEventListener("click", function (event) {
    if (event.target.classList.contains("dots__dot")) {
      const slide = event.target.dataset.slide;
      goToSlide(slide);
      activateDots(slide);
    }
  });
};
sliders();
////// LECTURES /////////
// add as sibling of header
// header.before(message);
// header.after(message);

// style
// message.style.backgroundColor = "#37383d";
// message.style.width = "120%";

// // delete header
// document
//   .querySelector(".btn--close--cookie")
//   .addEventListener("click", function () {
//     message.remove();
//   });

// // document.documentElement.style.setProperty("--color-primary", "orangered");

// const logo = document.querySelector(".nav__logo");
// console.log(logo.alt);
// console.log(logo.src);

// // setProperty
// logo.setAttribute("company", "bankist");

// // getAttribute
// console.log(logo.getAttribute("src"));

// // dataAttributes
// console.log(logo.dataset.versionNumber);

// // classList methods
// logo.classList.add("class-name", "class-name2");

// rightclick event
// document
//   .querySelector("body")
//   .addEventListener("contextmenu", function (event) {
//     event.preventDefault();
//     alert("⚠ You right clicked....");
//   });
// console.log();

// event propagation
// const randomInt = (min, max) => {
//   return Math.floor(Math.random() * (max - min + 1) + min);
// };
// const randomColor = () => {
//   return `rgb(${randomInt(0, 255)} , ${randomInt(0, 255)} , ${randomInt(
//     0,
//     255
//   )})`;
// };

// console.log(document.documentElement);
// console.log(document.head);
// console.log(document.body);

// const allSections = document.querySelectorAll(".section");
// console.log(allSections);
// const allButtons = document.getElementsByTagName("button");
// console.log(allButtons);

// querySelectorAll creates a NodeList which does not update
// getElementById(ClassName/TagName) creates an HTML Collection which can be updated by itself

// const message = document.createElement("div");
// message.classList.add("cookie-message");

// message.innerHTML =
//   'We use cookies for improved functionalities and analytics. <button class="btn btn--close--cookie">Got it!</button>';

// const header = document.querySelector(".header");
// // header.prepend(message);
// header.append(message); // add as child of header

// document
//   .querySelector(".nav__link")
//   .addEventListener("click", function (event) {
//     this.style.backgroundColor = randomColor();
//     console.log(this.style.backgroundColor);
//   });
// document
//   .querySelector(".nav__links")
//   .addEventListener("click", function (event) {
//     this.style.backgroundColor = randomColor();
//     console.log(this.style.backgroundColor);
//   });
// document.querySelector(".nav").addEventListener("click", function (event) {
//   this.style.backgroundColor = randomColor();
//   console.log(this.style.backgroundColor);
// });
