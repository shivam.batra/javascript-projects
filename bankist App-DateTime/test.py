def func1() :
    D = {1 : 'hello' , 2 : 'world'}
    print("In func1") 
    def func2() :
        print(f"Inside func2 : {locals()}")
        print("In func2")

    print(f"Before func2 : {D}")
    func2() 
    print(f"After func2 : {D}") 

func1()