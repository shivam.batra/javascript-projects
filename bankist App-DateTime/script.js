"use strict";

// Data

// DIFFERENT DATA! Contains movement dates, currency and locale

const account1 = {
  owner: "Jonas Schmedtmann",
  movements: [200, 455.23, -306.5, 25000, -642.21, -133.9, 79.97, 1300],
  interestRate: 1.2, // %
  pin: 1111,

  movementsDates: [
    "2021-08-23T21:31:17.178Z",
    "2021-08-22T07:42:02.383Z",
    "2021-05-28T09:15:04.904Z",
    "2021-02-01T10:17:24.185Z",
    "2020-10-22T14:11:59.604Z",
    "2020-03-20T17:01:17.194Z",
    "2020-01-11T23:36:17.929Z",
    "2019-12-12T10:51:36.790Z",
  ],
  currency: "EUR",
  locale: "pt-PT", // de-DE
};

const account2 = {
  owner: "Jessica Davis",
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,

  movementsDates: [
    "2019-11-01T13:15:33.035Z",
    "2019-11-30T09:48:16.867Z",
    "2019-12-25T06:04:23.907Z",
    "2020-01-25T14:18:46.235Z",
    "2020-02-05T16:33:06.386Z",
    "2021-08-22T14:43:26.374Z",
    "2021-06-25T18:49:59.371Z",
    "2021-07-26T12:01:20.894Z",
  ],
  currency: "USD",
  locale: "en-US",
};

const accounts = [account1, account2];

// Elements
const labelWelcome = document.querySelector(".welcome");
const labelDate = document.querySelector(".date");
const labelBalance = document.querySelector(".balance__value");
const labelSumIn = document.querySelector(".summary__value--in");
const labelSumOut = document.querySelector(".summary__value--out");
const labelSumInterest = document.querySelector(".summary__value--interest");
const labelTimer = document.querySelector(".timer");

const containerApp = document.querySelector(".app");
const containerMovements = document.querySelector(".movements");

const btnLogin = document.querySelector(".login__btn");
const btnTransfer = document.querySelector(".form__btn--transfer");
const btnLoan = document.querySelector(".form__btn--loan");
const btnClose = document.querySelector(".form__btn--close");
const btnSort = document.querySelector(".btn--sort");

const inputLoginUsername = document.querySelector(".login__input--user");
const inputLoginPin = document.querySelector(".login__input--pin");
const inputTransferTo = document.querySelector(".form__input--to");
const inputTransferAmount = document.querySelector(".form__input--amount");
const inputLoanAmount = document.querySelector(".form__input--loan-amount");
const inputCloseUsername = document.querySelector(".form__input--user");
const inputClosePin = document.querySelector(".form__input--pin");

// date formatting
const formatMovementDate = function (date, locale) {
  const calcDayPassed = (date1, date2) => {
    return Math.round(Math.abs(date1 - date2) / (1000 * 60 * 60 * 24));
  };
  const daysPassed = calcDayPassed(new Date(), date);
  if (daysPassed === 0) return "Today";
  if (daysPassed === 1) return "Yesterday";
  if (daysPassed <= 7) return `${daysPassed} days ago`;

  // const day = `${date.getDate()}`.padStart(2, 0);
  // const month = `${date.getMonth() + 1}`.padStart(2, 0);
  // const year = date.getFullYear();
  // return `${day}/${month}/${year}`;
  return new Intl.DateTimeFormat(locale).format(date);
};

// currency formatter
const formatCurr = function (value, locale, currency) {
  return new Intl.NumberFormat(locale, {
    style: "currency",
    currency: currency,
  }).format(value);
};

// display movements
const displayMovements = function (acc, sort = false) {
  containerMovements.innerHTML = "";
  const movs = sort
    ? acc.movements.slice().sort((a, b) => b - a)
    : acc.movements;
  movs.forEach(function (mov, i) {
    const type = mov > 0 ? "deposit" : "withdrawal";
    const date = new Date(acc.movementsDates[i]);
    const displayDate = formatMovementDate(date, acc.locale);

    const formattedMov = formatCurr(mov, acc.locale, acc.currency);

    const html = `
        <div class="movements__row">
            <div class="movements__type movements__type--${type}">${
      i + 1
    } ${type} </div>
            <div class="movements__date">${displayDate}</div>
            <div class="movements__value">${formattedMov}</div>
        </div>
        `;
    containerMovements.insertAdjacentHTML("beforeend", html);
  });
};

// calculate balance
const calculateBalance = function (account) {
  const balance = account.movements.reduce(function (acc, curr, i, arr) {
    return acc + curr;
  }, 0);
  labelBalance.textContent = formatCurr(
    balance,
    account.locale,
    account.currency
  );
  account.balance = balance;
};

// calculate summary
const calculateSummary = function (account) {
  // deposit summary
  const totalSumIn = account.movements
    .filter(function (mov) {
      return mov > 0;
    })
    .reduce(function (acc, mov) {
      return acc + mov;
    }, 0);
  labelSumIn.textContent = formatCurr(
    totalSumIn,
    account.locale,
    account.currency
  );
  //   withdrawl summary
  const totalSumOut = account.movements
    .filter(function (mov) {
      return mov < 0;
    })
    .reduce(function (acc, mov) {
      return acc + mov;
    }, 0);
  labelSumOut.textContent = formatCurr(
    totalSumOut,
    account.locale,
    account.currency
  );
  //   interests calculation

  const interest = account.movements
    .filter((mov) => mov > 0)
    .map((deposit) => (deposit * account.interestRate) / 100)
    .filter((int) => int >= 1)
    .reduce((acc, int) => acc + int, 0);

  labelSumInterest.textContent = formatCurr(
    interest,
    account.locale,
    account.currency
  );
};
// create usernames from names of account
const createUsername = function (accs) {
  accs.forEach(function (acc) {
    acc.user = acc.owner
      .toLowerCase()
      .split(" ")
      .map(function (name) {
        return name[0];
      })
      .join("");
  });
};
createUsername(accounts);

// update UI
const updateUI = function () {
  displayMovements(currentAcc);
  calculateBalance(currentAcc);
  calculateSummary(currentAcc);
};

// log out timer
const startLogOutTimer = function () {
  const tick = function () {
    const min = String(Math.trunc(time / 60)).padStart(2, 0);
    const sec = String(time % 60).padStart(2, 0);
    // in each call , print the remaining time in UI
    labelTimer.textContent = `${min}:${sec}`;
    // decrease one second

    // when 0 , stop timer and log out user
    if (time === 0) {
      clearInterval(timer);
      labelWelcome.textContent = "Login to Get Started";
      containerApp.style.opacity = 0;
    }
    time--;
  };
  // set time to five minutes
  let time = 300;
  tick();
  const timer = setInterval(tick, 1000);
};

// current account
let currentAcc, timer;
// get dates
const now = new Date();
const options = {
  hour: "numeric",
  minute: "numeric",
  day: "numeric",
  month: "numeric",
  year: "numeric",
};
const mylocale = navigator.language;

// event Handlers
btnLogin.addEventListener("click", function (event) {
  event.preventDefault();

  currentAcc = accounts.find((acc) => acc.user === inputLoginUsername.value);

  if (currentAcc?.pin === +inputLoginPin.value) {
    // display UI and welcome message
    labelWelcome.textContent = `Welcome Back, ${
      currentAcc.owner.split(" ")[0]
    }`;
    containerApp.style.opacity = 1;
    if (timer) clearInterval(timer);
    timer = startLogOutTimer();
    updateUI(currentAcc);

    // clear input field
    inputLoginPin.value = inputCloseUsername.value = "";
    inputClosePin.blur();
  }
});

// transfer mechanism
btnTransfer.addEventListener("click", function (event) {
  event.preventDefault();
  const amount = +inputTransferAmount.value;
  const receiverAcc = accounts.find(
    (acc) => acc.user === inputTransferTo.value
  );
  inputTransferTo.value = inputTransferAmount.value = "";
  if (
    amount > 0 &&
    receiverAcc &&
    amount <= currentAcc.balance &&
    currentAcc.user !== receiverAcc.user
  ) {
    currentAcc.movements.unshift(-amount);
    receiverAcc.movements.unshift(amount);
    // add transfer date
    currentAcc.movementsDates.unshift(new Date());
    receiverAcc.movementsDates.unshift(new Date());
    updateUI(currentAcc);
  }
});

btnClose.addEventListener("click", function (event) {
  event.preventDefault();

  if (
    inputCloseUsername.value === currentAcc.user &&
    +inputClosePin.value === currentAcc.pin
  ) {
    const index = accounts.findIndex((acc) => {
      return acc.user === currentAcc.user;
    });
    accounts.splice(index, 1);
    containerApp.style.opacity = 0;
  }
  inputCloseUsername.value = inputClosePin.value = "";
});

btnLoan.addEventListener("click", function (event) {
  event.preventDefault();
  const amount = +inputLoanAmount.value;
  setTimeout(function () {
    if (amount > 0 && currentAcc.movements.some((mov) => mov >= amount * 0.1)) {
      currentAcc.movements.unshift(amount);
      updateUI(currentAcc);
    }
  }, 2500);
  inputLoanAmount.value = "";
});

let sorted = false;
btnSort.addEventListener("click", function (event) {
  event.preventDefault();
  displayMovements(currentAcc, !sorted);
  sorted = !sorted;
});

/////////////////////// LECTURES //////////////////////////
// console.log(23 === 23.0);

// // parseInt
// // console.log(parseInt("20.5ecg"));
// // console.log(parseFloat("2.5rem"));
// console.log(Number.parseFloat("2.5rem"));
// // isNAN
// console.log(Number.isNaN(20));
// console.log(Number.isNaN("20"));
// console.log(Number.isNaN("24X"));
// console.log(Number.isNaN(+"24X")); //true
// // isFinite
// console.log(Number.isFinite(23 / 0));
// console.log(Number.isFinite(23));

// console.log("----------------------------------------");
// // modulus operator
// const isEven = (n) => n % 2 === 0;
// console.log(isEven(2));
// console.log(isEven(3));
// console.log(isEven(4));
// console.log(isEven(5));
// console.log(isEven(6));

// labelBalance.addEventListener("click", function () {
//   [...document.querySelectorAll(".movements__row")].forEach(function (row, i) {
//     console.log("click");
//     if (i % 2 === 0) row.style.backgroundColor = "orangered";
//   });
// });
// console.log(2 ** 53 - 1);
// console.log(Number.MAX_SAFE_INTEGER);
// // in pre es2020 biggest safe number was 9007199254740991 = (2**53 - 1)
// console.log(BigInt(2349239478324728473847234847284));
// console.log(2349239478324728473847234847284n);

// today = new Date(2021, 0, 23);
// console.log(new Date());

// const num = 10000000000000;
// const op = {
//   style: "currency",
//   unit: "celsius",
//   currency: "INR",
// };
// console.log("India : ", new Intl.NumberFormat("hi-IN", op).format(num));
// console.log("US : ", new Intl.NumberFormat("en-US", op).format(num));
// console.log("Germany : ", new Intl.NumberFormat("de-DE", op).format(num));
// console.log("Syria : ", new Intl.NumberFormat("ar-SY", op).format(num));

// setTimeout(
//   function (ing1, ing2) {
//     console.log(`Here is your pizza 🍕 with ${ing1} & ${ing2}`);
//   },
//   3000,
//   "olive",
//   "pepperoni"
// );

// setInterval
// setInterval(function () {
//   const now = new Date();
//   console.log(now);
// }, 1000);
